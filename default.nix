{ lib
#, llvmPackages_14
, stdenv
, cmake
, spdlog
, libyamlcpp
, boost180
, gtest
, protobuf
, antlr4
, jre_minimal
, commonroad-cmake
, rvmonitors
#, git
# , abseil-cpp
}:
let antlr = antlr4.override { jre = jre_minimal; };

#in llvmPackages_14.stdenv.mkDerivation rec {
in stdenv.mkDerivation rec {
  pname = "rvmonitors";
  version = "0.1.0";
  
  src = ./.;

  nativeBuildInputs = [
    cmake
    jre_minimal
  ];
  buildInputs = [
    #spdlog
    # boost180
    gtest
    gtest.dev
    # llvmPackages_14.openmp
    antlr
    antlr.runtime.cpp
    antlr.runtime.cpp.dev
    #abseil-cpp 
    ];

  cmakeFlags = [
    "-DFETCHCONTENT_QUIET=OFF"
    "-DFETCHCONTENT_SOURCE_DIR_COMMONROAD_CMAKE=${commonroad-cmake}"
    "-DANTLR4_JAR_LOCATION=${antlr.jarLocation}"
  ];

  meta = with lib; {
    homepage = "https://github.com/nixvital/nix-based-cpp-starterkit";
    description = ''
      A template for Nix based C++ project setup.";
    '';
    licencse = licenses.mit;
    platforms = with platforms; [linux darwin];
    #maintainers = [ maintainers.breakds ];
  };
}
