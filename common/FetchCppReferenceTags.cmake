include(FetchContent)

# Current release: 20190607 (update if required)
# Check on https://en.cppreference.com/w/Cppreference:Archives
FetchContent_Declare(
  cppreference_tags
  URL      https://github.com/PeterFeicht/cppreference-doc/releases/download/v20220730/html-book-20220730.tar.xz
  URL_HASH SHA256=71f15003c168b8dc5a00cbaf19b6480a9b3e87ab7e462aa39edb63d7511c028b
)


FetchContent_MakeAvailable(cppreference_tags)

message(VERBOSE "looking for cppreference tags in ${cppreference_tags_SOURCE_DIR}")

# Pass path to cppreference tag file
find_file(CPPREFERENCE_TAG_FILE
  NAMES cppreference-doxygen-web.tag.xml
  PATHS ${cppreference_tags_SOURCE_DIR}
  )
message(STATUS "cppreference tags available at ${CPPREFERENCE_TAG_FILE}")
