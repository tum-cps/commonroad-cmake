include_guard(DIRECTORY)

set(ALLOWED_SYSTEM_LIBRARIES "") # CACHE INTERNAL "")

function(check_system_library system_library_name)
    message(VERBOSE "finding ${system_library_name}")
    #set(sln lib-${system_library_name})
    find_library(sl ${system_library_name} NO_CACHE)
    message(VERBOSE "result: ${sl}")
    if (${sl} STREQUAL "sl-NOTFOUND")
        return()
    endif()
    list(APPEND ALLOWED_SYSTEM_LIBRARIES ${sl})
    set(ALLOWED_SYSTEM_LIBRARIES ${ALLOWED_SYSTEM_LIBRARIES} PARENT_SCOPE)
endfunction()

foreach(system_library_name IN ITEMS libc.so.6 libpthread.so.0 libm.so.6 libgcc_s.so.1 libstdc++.so.6 ld-linux-x86-64.so.2)
    check_system_library(${system_library_name})
endforeach()

message(VERBOSE "System library list: ${ALLOWED_SYSTEM_LIBRARIES}")