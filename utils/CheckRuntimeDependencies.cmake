include_guard(DIRECTORY)

message(STATUS "CMAKE MODULE - CSD is ${CMAKE_CURRENT_LIST_DIR}")

include(${CMAKE_CURRENT_LIST_DIR}/../common/DiscoverSystemLibraries.cmake)

function(check_python_runtime_dependencies MODULE)
    #https://cmake.org/cmake/help/latest/variable/CMAKE_CURRENT_FUNCTION_LIST_DIR.html
    #requires 3.17
    configure_file(
        ${CMAKE_CURRENT_FUNCTION_LIST_DIR}/CheckRuntimeDependenciesCommon.cmake.in
        ${CMAKE_CURRENT_BINARY_DIR}/CheckRuntimeDependenciesCommon.cmake
        @ONLY
    )

    file(GENERATE
        OUTPUT CheckRuntimeDependencies-${MODULE}.cmake
        CONTENT "include(${CMAKE_CURRENT_BINARY_DIR}/CheckRuntimeDependenciesCommon.cmake)\ncheck_target_rdeps($<TARGET_FILE:${python_module_name}>)\n"
        TARGET ${MODULE}
        )

    install(SCRIPT ${CMAKE_CURRENT_BINARY_DIR}/CheckRuntimeDependencies-${MODULE}.cmake COMPONENT PythonModules)
endfunction()
