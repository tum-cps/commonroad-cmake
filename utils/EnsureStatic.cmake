function(collect_link_libraries LIBRARY RESULT)
list(APPEND CMAKE_MESSAGE_CONTEXT "collect_link_libraries")

set(TODO "")
list(APPEND TODO ${LIBRARY})

set(LIBRARIES "")
set(CHECKED "")
set(SKIPPED "")

while(TODO STRGREATER "")
    list(POP_FRONT TODO T_FRONT)

    list(FIND CHECKED ${T_FRONT} CHECKED_IDX)
    if(NOT CHECKED_IDX EQUAL -1)
            message(VERBOSE "skip \"${T_FRONT}\", already checked")
            continue()
    endif()
    list(APPEND CHECKED ${T_FRONT})

    if(T_FRONT MATCHES "^\\$<BUILD_INTERFACE:([^>]+)>$")
            message(VERBOSE "replace \"${T_FRONT}\" with \"${CMAKE_MATCH_1}\"")
            set(T_FRONT ${CMAKE_MATCH_1})
    endif()

    message(TRACE "checking \"${T_FRONT}\"")

    if(NOT TARGET ${T_FRONT})
            message(VERBOSE "skip \"${T_FRONT}\", not a target")
            #find_library(${T_FRONT})
            list(APPEND SKIPPED ${T_FRONT})
            continue()
    endif()

    list(APPEND LIBRARIES ${T_FRONT})

    get_target_property(T_LL ${T_FRONT} LINK_LIBRARIES)
    if(T_LL STREQUAL "T_LL-NOTFOUND")
            message(VERBOSE "skip \"${T_FRONT}\", no link libraries")
            continue()
    endif()

    message(VERBOSE "link libraries of \"${T_FRONT}\": ${T_LL}")

    list(APPEND TODO ${T_LL})

    message(TRACE "remaining: ${TODO}")
    message(TRACE "libraries: ${LIBRARIES}")
    message(TRACE "checked: ${CHECKED}")
endwhile()
message(TRACE "libraries: ${LIBRARIES}")
message(TRACE "checked: ${CHECKED}")
message(TRACE "skipped: ${SKIPPED}")

list(REMOVE_DUPLICATES LIBRARIES)

set(${RESULT} ${LIBRARIES} PARENT_SCOPE)
endfunction()

function(ensure_static LIBRARY)
list(APPEND CMAKE_MESSAGE_CONTEXT "ensure_static")

get_target_property(LIBRARY_TYPE ${LIBRARY} TYPE)
message(VERBOSE "\"${LIBRARY}\" has type ${LIBRARY_TYPE}")

foreach(ALLOWED_TYPE IN ITEMS OBJECT_LIBRARY INTERFACE_LIBRARY UNKNOWN_LIBRARY)
    if(LIBRARY_TYPE STREQUAL ${ALLOWED_TYPE})
            message(VERBOSE "skip \"${LIBRARY}\", ${LIBRARY_TYPE} is allowed")
            return()
    endif()
endforeach()

if(NOT LIBRARY_TYPE STREQUAL STATIC_LIBRARY)
    message(SEND_ERROR "PYTHON MODE - expected \"${LIBRARY}\" to be built as a static library, "
      "but its type is ${LIBRARY_TYPE}. \n"
      "Our Python build system expects that the Python modules do not have any additional "
      "shared library dependencies. "
      "Deviating from this expectation may cause hard-to-find bugs in the Python build "
      "process and is therefore unsupported. \n"
      "Please carefully check whether you may have inadvertently modified any CMake scripts "
      "or accidentally set BUILD_SHARED_LIBS to ON.")
endif()
endfunction()

function(ensure_all_static LIBRARY)
list(APPEND CMAKE_MESSAGE_CONTEXT "ensure_all_static")

#set(LL "")
collect_link_libraries(${LIBRARY} LL)
message(VERBOSE "LL: ${LL}")

foreach(llib ${LL})
    ensure_static(${llib})
endforeach()
endfunction()