include(CheckCXXCompilerFlag)
include(CheckCXXSourceCompiles)

set(COMMONROAD_SANITIZERS OFF CACHE STRING "Sanitizers to enable")
set_property(CACHE COMMONROAD_SANITIZERS PROPERTY
    STRINGS
    OFF ASAN UBSAN ASAN_UBSAN)

function(test_sanitizer_flag sanitizer ret_variable)
    set(TRIVIAL_MAIN "int main() { return 0; }")
    set(CMAKE_REQUIRED_FLAGS -fsanitize=${sanitizer})
    set(CMAKE_REQUIRED_LINK_OPTIONS -fsanitize=${sanitizer})
    check_cxx_source_compiles("${TRIVIAL_MAIN}" "${ret_variable}")
endfunction()

test_sanitizer_flag("address" ASAN_SUPPORT)
test_sanitizer_flag("undefined" UBSAN_SUPPORT)
test_sanitizer_flag("address,undefined" ASAN_UBSAN_SUPPORT)

if (NOT ("${COMMONROAD_SANITIZERS}" STREQUAL "OFF")
    AND
    (("${CMAKE_BUILD_TYPE}" STREQUAL "Release") OR ("${CMAKE_BUILD_TYPE}" STREQUAL "RelWithDebInfo")))
    message(WARNING "ASan/UBSan enabled in release mode!")
endif ()

if (("${COMMONROAD_SANITIZERS}" STREQUAL "ASAN")
    AND
    NOT ("${ASAN_SUPPORT}"))
    message(WARNING "ASan enabled, but ASan compiler check failed")
endif ()

if (("${COMMONROAD_SANITIZERS}" STREQUAL "UBSAN")
    AND
    NOT ("${UBSAN_SUPPORT}"))
    message(WARNING "ASan enabled, but ASan compiler check failed")
endif ()

if (("${COMMONROAD_SANITIZERS}" STREQUAL "ASAN_UBSAN")
    AND
    NOT ("${ASAN_UBSAN_SUPPORT}"))
    message(WARNING "ASan and UBSan enabled, but combined ASan/UBSan compiler check failed")
endif ()

# When building mixed executables (containing libraries built without ASan),
# -asan-use-private-alias=1 might be required in order to prevent false-positive
# warnings about ODR (one definition rule) violations
set(clang_odr_fix "$<$<COMPILE_LANG_AND_ID:CXX,Clang>:-mllvm;-asan-use-private-alias=1>")

set(enable_asan "$<AND:$<STREQUAL:${COMMONROAD_SANITIZERS},ASAN>,$<BOOL:${ASAN_SUPPORT}>>")
set(enable_ubsan "$<AND:$<STREQUAL:${COMMONROAD_SANITIZERS},UBSAN>,$<BOOL:${UBSAN_SUPPORT}>>")
set(enable_asan_ubsan "$<AND:$<STREQUAL:${COMMONROAD_SANITIZERS},ASAN_UBSAN>,$<BOOL:${ASAN_UBSAN_SUPPORT}>>")
set(need_odr_fix "$<OR:${enable_asan},${enable_asan_ubsan}>")

add_compile_options("$<${enable_asan}:-fsanitize=address>")
add_compile_options("$<${enable_ubsan}:-fsanitize=undefined>")
add_compile_options("$<${enable_asan_ubsan}:-fsanitize=address,undefined>")
add_link_options("$<${enable_asan}:-fsanitize=address>")
add_link_options("$<${enable_ubsan}:-fsanitize=undefined>")
add_link_options("$<${enable_asan_ubsan}:-fsanitize=address,undefined>")

add_compile_options("$<${need_odr_fix}:${clang_odr_fix}>")
