include_guard(DIRECTORY)

# Add a proper target for dl and gcov

# We use LINK_LIBRARIES_ONLY_TARGETS in order to only allow targets to be specified in target_link_libraries.
# However, there is no proper target for libdl and gcov as they is provided by the toolchain (GCC/glibc) itself.
# We create toolchain libraries in order to still have a proper targets for gcov and dl.
# See https://cmake.org/cmake/help/v3.25/prop_tgt/LINK_LIBRARIES_ONLY_TARGETS.html

add_library(toolchain::dl INTERFACE IMPORTED)
set_property(TARGET toolchain::dl PROPERTY IMPORTED_LIBNAME ${CMAKE_DL_LIBS})

add_library(toolchain::gcov INTERFACE IMPORTED)
set_property(TARGET toolchain::gcov PROPERTY IMPORTED_LIBNAME "gcov")
add_library(gcov ALIAS toolchain::gcov)

find_library(libatomic atomic)
add_library(toolchain::atomic INTERFACE IMPORTED)
if(NOT (libatomic MATCHES "libatomic-NOTFOUND"))
    set_property(TARGET toolchain::atomic PROPERTY IMPORTED_LOCATION ${libatomic})
else()
    set_property(TARGET toolchain::atomic PROPERTY IMPORTED_LIBNAME "atomic")
endif()
add_library(atomic ALIAS toolchain::atomic)