# Check whether the system supports linking using the LLD linker
# Using LLD improves link times significantly

include(CheckLinkerFlag)

function(check_lld_mold_supported lang)
    get_cmake_property(cmake_languages ENABLED_LANGUAGES)

    if(NOT ${lang} IN_LIST cmake_languages)
        message(VERBOSE "Language ${lang} is not enabled - not checking lld/mold support")
        return()
    endif()

    if(NOT DEFINED LINKER_${lang}_LLD_SUPPORT)
        message(STATUS "Checking lld support for ${lang}")
        check_linker_flag(${lang} -fuse-ld=lld LINKER_${lang}_LLD_SUPPORT)
    endif()

    if(NOT DEFINED LINKER_${lang}_MOLD_SUPPORT)
        message(STATUS "Checking mold support for ${lang}")
        check_linker_flag(${lang} -fuse-ld=mold LINKER_${lang}_MOLD_SUPPORT)
    endif()

    if(LINKER_${lang}_MOLD_SUPPORT)
        message(STATUS "Using mold for linking ${lang}")
        add_link_options($<$<BOOL:$<LINK_LANGUAGE:${lang}>>:-fuse-ld=mold>)
    elseif(LINKER_${lang}_LLD_SUPPORT)
        message(STATUS "Using lld for linking ${lang}")
        add_link_options($<$<BOOL:$<LINK_LANGUAGE:${lang}>>:-fuse-ld=lld>)
    endif()
endfunction()

check_lld_mold_supported(C)
check_lld_mold_supported(CXX)
